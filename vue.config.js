/*
 * @description:
 * @Date: 2023-01-31 14:03:11
 * @example:
 * @params:
 */
const { defineConfig } = require("@vue/cli-service");
// const ESLintPlugin = require("eslint-webpack-plugin");
// const plugins = [new ESLintPlugin()];
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: true,
  // plugins,
  // configureWebpack:{
  //   plugins
  // }
});
