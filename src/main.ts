/*
 * @description: Main.ts
 * @Date: 2023-01-31 14:03:11
 * @example:
 * @params:
 */
import { createApp,defineComponent } from "vue";
import ElementPlus from "element-plus";
import App from "./App.vue";
import pinia from "./stores";
import router from "./config/router";

import VueGridLayout from 'vue-grid-layout'
import Vue3ColorPicker from "vue3-colorpicker";
import "vue3-colorpicker/style.css";
import "../public/mapbox-gl.css"
/* vue3 */
import vue3dLoader from "vue-3d-loader";
import "element-plus/dist/index.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(pinia).use(Vue3ColorPicker).use(vue3dLoader).use(VueGridLayout).use(router).use(ElementPlus).mount("#app");
