/*
 * @description: 封装的请求方法
 * @Date: 2023-01-31 17:47:52
 * @example:
 * @params:
 */
import fetchTools from "./fetchTools";

export function axiosGet( url: string, params?: object) {
  return fetchTools.fetch({
    url,
    method: "get",
    params,
  });
}

export function axiosDelete(url: string, params?: object) {
  return fetchTools.fetch({
    url,
    method: "delete",
    params,
  });
}

export async function axiosPost(url: string, params?: object) {
  const res: any = await fetchTools.fetch({
    url,
    method: "post",
    params,
  });
  return res;
}

export async function axiosPut(url: string, data?: object) {
  const res = await fetchTools.fetch({
    url,
    method: "put",
    data,
  });

  return res;
}
