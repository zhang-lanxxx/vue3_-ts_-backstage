/*
 * @description: 
 * @Date: 2023-01-31 17:40:09
 * @example: 
 * @params: 
 */
import axios from "axios";
import { ElMessage, ElLoading } from "element-plus";

const services = axios.create({
  baseURL:"http://map.anrookie.cn:3000"
})

// 添加响应拦截器
services.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    ElMessage({
      message: error.message,
      showClose: true,
      duration: 3,
      type: "error",
    });
    return Promise.reject(error);
  }
);

interface AxiosOption {
  url: string;
  method: string;
  baseURL?: string;
  params?: {};
  data?: any;
}

export default {
  basics(option: AxiosOption) {
    return services(option);
  },
  fetch(option: AxiosOption) {
    const loading = ElLoading.service({
      lock: true,
      text: "Loading",
      background: "rgba(0,0,0,0.7)",
    });
    return this.basics(option).finally(() => loading.close());
  },
};
