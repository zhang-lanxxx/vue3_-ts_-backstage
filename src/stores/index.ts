/*
 * @description: 配置 pinina
 * @Date: 2023-01-31 17:57:52
 * @example:
 * @params:
 */
import { createPinia } from "pinia";
import { createPersistedState } from "pinia-plugin-persistedstate";
const pinia = createPinia();

pinia.use(
  createPersistedState({
    serializer: {
      // 指定参数序列化器
      serialize: JSON.stringify,
      deserialize: JSON.parse,
    },
  })
);
export function setupStore(app: any) {
  app.use(pinia);
}
export default pinia;
