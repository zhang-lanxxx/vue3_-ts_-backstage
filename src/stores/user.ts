/*
 * @description:
 * @Date: 2023-02-14 11:16:35
 * @example:
 * @params:
 */
import { defineStore } from "pinia";

/** 匿名抛出 */
export default  defineStore("Userinfo", {
  // 为了完整类型推理，推荐使用箭头函数
  state: () => {
    return {
      // 所有这些属性都将自动推断出它们的类型
      Userinfo: {
        avatar: "test",
        nickname: "test",
        token: "test",
        fileName: "test",
      },
    };
  },
  getters: {},
  // 同步和异步都可以
  actions: {},
});
