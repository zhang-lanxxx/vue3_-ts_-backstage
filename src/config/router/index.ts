/*
 * @description: 路由树渲染侧边栏
 * @Date: 2023-01-31 14:03:11
 * @example:
 * @params:
 */
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Layout from "@/layout/index.vue";
// import { h } from 'vue'

// const About = {
//   render () {s
//     return h('div', 'About')
//   }
// }

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: () => import("@/views/RootHome.vue"),
    name:"login",
    meta: { isShow: false },
  },
  {
    path: "/applist",
    component: Layout,
    name: "我的应用",
    meta: {
      icon: "Monitor",
    },
    children: [
      {
        path: "/applist/home",
        name: "工作台",
        meta: { title: "工作台" },
        component: () => import("@/views/chapsick.vue"),
      },
      {
        path: "/applist/appL",
        name: "我的博客",
        meta: { title: "我的博客" },
        component: () => import("@/views/appstore/index.vue"),
      },
      {
        path: "/applist/addApp",
        name: "数据列表",
        meta: { title: "数据列表" },
        component: () => import("@/views/appstore/components/addEditApp.vue"),
      },
      {
        path: "/applist/appDetail",
        name: "应用详情",
        meta: { title: "应用详情" },
        component: () => import("@/views/appstore/components/appDetail.vue"),
      },
      {
        path: "/applist/appVersion",
        name: "地图预览",
        meta: { title: "地图预览" },
        component: () => import("@/views/appstore/components/appVersion.vue"),
      },
      // 拖拽布局
      {
        path: "/applist/Layout",
        name: "Dashbord",
        meta: { title: "DIY Dashbord" },
        component: () => import("@/views/project/index.vue"),
      },
      // 地图打点
      {
        path: "/applist/PointMap",
        name: "PointMap",
        meta: { title: "PointMap" },
        component: () => import("@/views/MapGenerator/PointMap.vue"),
      },
      // 3D 模型展示 Vue 3D Model 
      {
        path: "/applist/3DModel",
        name: "3Dmodel",
        meta:{title: "3Dmodel" },
        component: ()=> import("@/views/3dModel/index.vue")
      },
      // webGL Unity 
      {
        path: "/applist/Unity",
        name: "Unity",
        meta:{title: "Unity" },
        component: ()=> import("@/views/Unity/index.vue")
      }
    ],
  },
  /** Person 页  */
  {
    path: "/person",
    component: Layout,
    name: "person",
    meta: {
      icon: "Money",
    },
    children:[
      {
        path: "/person/CSDN",
        name: "CSDN",
        meta: { title: "CSDN" },
        component: () => import("@/views/personal/Mygitee.vue"),
      },
      {
        path: "/person/perdetail",
        name: "个人信息",
        meta: { isShow: false ,title: "个人信息"},
        component: () => import("@/views/personal/personInfo/index.vue"),
      }
    ]
  },
  /** APP传感器详情 */
  {
    path: "/Sensor/:title",
    component: () => import("@/views/appstore/demo/SensorSorce.vue"),
    name:"SensorInfo",
    meta: { isShow: false },
  },

];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to,from,next)=>{
  let  token = window.sessionStorage.getItem('token') || ''
  if (to.name !== "login" && !token) {
    next("/");
  } else {
    next();
  }
})

export default router;
