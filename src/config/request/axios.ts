/*
 * @description:
 * @Date: 2023-01-31 17:33:58
 * @example:
 * @params:
 */
import axios from "axios";

// 添加请求拦截器
axios.defaults.timeout = 60000;
axios.interceptors.request.use(
  (_config: any) => {
    const config = _config;
    const obj: any = window.sessionStorage.getItem("app");
    const { token } = JSON.parse(obj) || "";
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error: any) => {
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  (response: any) => {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    // console.log(response, "1321");
    return response;
  },
  (error: any) => {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);
