/*
 * @description:
 * @Date: 2023-03-01 11:54:05
 * @example:
 * @params:
 */
import { axiosGet } from "@/tools/request";

const Testfn = () => {
  return axiosGet("/a/a/a", { username: "zhangkai" });
};

/** 改造 */
const getSensorList = (params) => {
  return axiosGet("/SensorList",params);
}

export default {
  Testfn,
  getSensorList
};
