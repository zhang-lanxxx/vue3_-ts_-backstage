/*
 * @description:
 * @Date: 2023-05-10 14:49:22
 * @example:
 * @params:
 */
export default  DeviceCoord = {
  HR: [
    {
      coord: [396.243151570349, 670.1259605078776],
      data: [
        "70b3d52c0001ee64/CLPOcc_069",
        "DeskOccupancy_Presence",
        "1.0",
        "01/CLP/3/HR/CLPOcc_069/Presence",
      ],
      id: "70b3d52c0001ee64/CLPOcc_069",
      unitId: 0,
    },
    {
      coord: [364.0817373461012, 99.47560419516641],
      data: [
        "70b3d52c0001ee60/CLPOcc_100",
        "DeskOccupancy_Presence",
        "0.0",
        "01/CLP/3/HR/CLPOcc_100/Presence",
      ],
      id: "70b3d52c0001ee60/CLPOcc_100",
      unitId: 0,
    },
  ],
};
