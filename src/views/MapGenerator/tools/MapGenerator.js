import * as echarts from "echarts";
import axios from "axios";

const instance = axios.create({
  timeout: 60000,
});

export default class MapGenerator {
  constructor(dom, ID, size = 15) {
    this.ID = ID;
    // field 样式列表
    this.style = [
      "#ace03d",
      "#c263da",
      "#d2693c",
      "#84a7dd",
      "#e5b962",
      "#cd4b22",
      "#c7955b",
      "#4e5ffc",
      "#48ae2e",
      "#e0e0e0",
      "#9564bc",
      "#f1df38",
      "#27d14d",
      "#79e3ea",
    ];
    this.size = size;
    this.zoom = 2;
    this.graphicsDataArr = [];
    this.graphic = [];
    this.fPlanB64 = null;

    this.chart = echarts.init(dom, null, {
      renderer: "svg",
    });

    // echarts  基础配置 
    this.option = {
      title: {},
      geo: {
        tooltip: {},
        map: `floorPlan${this.ID}`,
        zoom: 2,
        roam: true,
        emphasis: {},
        animationDurationUpdate: 0,
      },
      grid: {
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
      },
      tooltip: {
        appendToBody: true,
      },
      series: [],
      graphic: [],
    };
    this.chart.clear();
    echarts.registerMap(`floorPlan${ID}`, { svg: "" });
    this.chart.setOption(this.option);
    if (this.fPlanB64) {
      this.initMap();
    }
  }

  initMap() {
    instance.get(this.fPlanB64).then(({ data: svg }) => {
      this.showFP(svg);
    });
  }

  showFP(svg, option = this.option) {
    this.graphicsDataArr = [];
    this.chart.clear();
    echarts.registerMap(`floorPlan${this.ID}`, { svg });
    this.chart.setOption(option);
    this.chart.on("georoam", (params) => {
      if (params.dy || params.dx) {
        this.setGraphicsDataArr(this.graphicsDataArr);
      } else {
        const zoom = params.zoom > 1 ? this.zoom + 0.1 : this.zoom - 0.1;
        this.setZoom(zoom);
      }
    });

    this.chart.resize();
  }

  setSvg(svg) {
    this.fPlanB64 = svg;
    this.initMap();
  }

  setTitleText() {
    this.chart.setOption(this.option, true);
    return this;
  }

  setZoom(zoom) {
    const val = zoom > 0.6 ? zoom : 0.6;
    this.option.geo.zoom = val;
    this.zoom = val;
    this.chart.setOption(this.option, true);
    this.setGraphicsDataArr(this.graphicsDataArr);
    return this;
  }

  setGraphics(data = []) {
    this.option.graphic = data;
    this.graphic = data;
    // const graphic = data;
    this.chart.setOption(this.option, {
      replaceMerge: ["graphic"],
    });
    return this;
  }

  setGraphicsDataArr(data,zoom,) {
    this.graphicsDataArr = data || [];
    if (zoom) {
      this.setZoom(zoom);
    } else {
      this.setGraphics(
        data.map((item, index) => this.toPointData(item, index,))
      );
    }
    return this;
  }

  // 渲染点位
  toPointData(_item,index,) {
    const item = _item;
    const that = this;
    const { zoom } = this;
    item.unitId = item.unitId ? item.unitId : 0;
    const { coord } = item;
    const positionSize = this.size * zoom;

    const position = this.chart.convertToPixel("geo", coord);
    // const imageName = item.data[2] === "1.0" ? "2" : "0";
    return {
      name: index,
      type: "image",
      description: "",
      deviceType: "1",
      x: position[0] || positionSize,
      y: position[1] || positionSize,
      z: 3,
      draggable: item.draggable,
      style: {
        image: require(`../svg/icon2.svg`),
        width: positionSize,
        height: positionSize,
      },
      tooltip: {
        trigger: "item",
        position(point) {
          const pointX = point[0];
          const pointY = point[1];
          const x = pointX + positionSize;
          const y = pointY;
          return [x, y];
        },
        confine: true,
        padding: [4, 8, 4, 8],
        formatter: () => {
          const itemValue = item.data;
          if (JSON.stringify(itemValue) === "{}") return "";
          return `<div class="map-tooltip">
            <div class="title">
              ${itemValue[0]}
            </div>
          </div>`;
        },
        backgroundColor: "rgba(28, 35, 34, 0.9)",
        textStyle: {
          color: "#F2F2F2",
        },
      },
      // 拖拽点位
      ondrag() {
        const co = that.chart.convertFromPixel({ geoIndex: 0 }, [
          this.x,
          this.y,
        ]);
        item.coord = co;

        item.name = item.name ? item.name : "";
        that.graphic[index].coord = co;
        console.log("yoo | =========>>co", co);
      },
      coord: item.coord,
    };
  }

  // 获取 field 对应的样式
  getFieldStyle(index) {
    const num = index % 14;
    return `color: ${this.style[num]}`;
  }

  showLoading() {
    this.chart.showLoading({
      text: "loading",
      color: "#FFFFFF",
      textColor: "#FFFFFF",
      maskColor: "rgba(0, 0, 0, 0.8)",
      zlevel: 0,
    });
  }

  hideLoading() {
    this.chart.hideLoading();
  }

  resize() {
    this.chart.resize();
    return this;
  }
}
