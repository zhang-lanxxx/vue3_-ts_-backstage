/*
 * @description: 
 * @Date: 2023-04-07 11:06:33
 * @example: 
 * @params: 
 */
import { axiosGet,axiosPost } from "@/tools/request";

/** layList */
const getWidgetList = () => {
  return axiosGet("/LayoutList");
}

/** update */
const handleUpdataLayout = (data) => {
    return axiosGet("/UpdateLayout",data);
}

/** Add  */
const handleAddLayout = (data) => {
  return axiosPost('/AddLayoutComs',data)
}

const handleDelLatout = (data) => {
  return axiosGet('/DelLayoutComs',data)
  
}

/** Edit  */
const handleEditLayout = (data) => {
  return axiosPost('/EditLayout',data)
}
export default {
    getWidgetList,
    handleUpdataLayout,
    handleAddLayout,
    handleDelLatout,
    handleEditLayout
};