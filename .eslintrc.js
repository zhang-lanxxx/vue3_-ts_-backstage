/*
 * @description:
 * @Date: 2023-02-22 13:41:42
 * @example:
 * @params:
 */
// module.exports = {
//   env: {
//     browser: true,
//     es2021: true,
//     node: true,
//     amd: true,
//     commonjs: true,
//   },
//   extends: [
//     "plugin:vue/vue3-essential", // vue3
//     // "plugin:vue/essential", // vue2
//     "airbnb-base",
//     "plugin:prettier/recommended",
//     "plugin:import/recommended", // ***1.解决引入问题
//   ],
//   globals: {
//     ApolloConfig: "readonly",
//     myVue: "readonly",
//   },
//   overrides: [],
//   parserOptions: {
//     ecmaVersion: "latest",
//     sourceType: "module",
//   },
//   plugins: ["vue"],
//   settings: {
//     "import/resolver": {
//       alias: {
//         map: [["@", "./src"]],
//         extensions: [".js", ".jsx", ".vue"],
//       },
//     },
//   },
//   rules: {
//     "no-console": "off",
//     "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
//     "vue/multi-word-component-names": "off",
//     /* import 省略后缀名 */
//     "import/extensions": [
//       "error",
//       "always",
//       { js: "never", jsx: "never", vue: "never" },
//     ],
//     "no-param-reassign": [
//       "error",
//       {
//         props: true,
//         ignorePropertyModificationsFor: [
//           "state", // for vuex state
//         ],
//       },
//     ],
//     "global-require": "on", // 允许 在局部使用require 加载资源
//     "import/no-dynamic-require": "on", // 允许require 动态加载资源
//   },
// };
