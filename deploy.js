/*
 * @description: 自动化构建
 * @Date: 2023-02-14 14:45:50
 * @example:
 * @params:
 */
const client = require("scp2");
const ora = require("ora");
const chalk = require("chalk");
const spinner = ora(chalk.green("正在发布到测试服务器..."));
spinner.start();

client.scp(
  "./dist",
  {
    // 本地打包文件的位置np
    host: "47.115.225.183", // IP地址
    port: "22", // 服务器端口
    username: "root", // 用户名
    password: "Zhangkai520.", // 密码
    path: "/www/wwwroot/zhang.anrookie.cn", // 项目路径
  },
  (err) => {
    spinner.stop();
    if (!err) {
      console.log(chalk.green("测试服务器部署完毕。"));
    } else {
      console.log("err", err);
    }
  }
);
